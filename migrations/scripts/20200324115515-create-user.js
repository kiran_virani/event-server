'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      await queryInterface.addColumn('user', 'reset_token', {
        type: Sequelize.STRING
      }),
      await queryInterface.addColumn('user', 'reset_token_expiry', {
        type: Sequelize.DATE
      }),
    ];
  },

  down: async (queryInterface, Sequelize) => {
    return [
     
    ];
  }
};