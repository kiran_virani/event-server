// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;
const sequelizeClient = require('../sequelize-client');

const Event = sequelizeClient.define('event', {
  user_id: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  event_name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  event_date: {
    type: DataTypes.DATE,
    allowNull: false
  },
  description: {
    type: DataTypes.STRING,
    allowNull: false
  },
  invited_users: {
    type: DataTypes.ARRAY(DataTypes.INTEGER),
    defaultValue: []
  }
}, {
  hooks: {
    beforeCount(options) {
      options.raw = true;
    }
  },
  tableName: 'event',
  paranoid: true,
  timestamps: true,
  underscored: true
});

Event.associate = function (models) {
  Event.belongsTo(models.user, { as: 'user', foreignKey: 'user_id', targetKey: 'id', constraints: false });
};

module.exports = Event;
