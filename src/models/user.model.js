// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;
const sequelizeClient = require('../sequelize-client');

const User = sequelizeClient.define('user', {
  first_name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  last_name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false
  },
  reset_token: {
    type: DataTypes.STRING,
    required: false
  },
  reset_token_expiry: {
    type: DataTypes.DATE,
    required: false
  },
}, {
  hooks: {
    beforeCount(options) {
      options.raw = true;
    }
  },
  tableName: 'user',
  paranoid: true,
  timestamps: true,
  underscored: true
});

module.exports = User;
