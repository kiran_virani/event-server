const moment = require('moment');

const verifyResetToken = async (req, res, next) => {
  try {
    const { query } = req;
    const sequelize = req.app.get('sequelizeClient');
    const {
      user: UserModel
    } = sequelize.models;
    const { uid, token } = query;
    
    const userInstance = await UserModel.findByPk(uid);
    if (!userInstance || !token || (userInstance.reset_token != token)) {
      return res.status(403).json({code:'INVALID_LINK'});
    }
    if (moment().isAfter(moment(userInstance.reset_token_expiry))) {
      return res.status(403).json({code:'RESET_PASSWORDLINK_EXPIRED'});
    }

    let successObj = {
      status: 'SUCCESS',
      message: 'VALID_LINK'
    };

    return res.status(200).json(successObj);
  } catch (exec) {
    return next(exec);
  }
};

module.exports = verifyResetToken;
