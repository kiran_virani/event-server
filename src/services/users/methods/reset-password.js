const moment = require('moment');
const UserModel = require('../../../models/user.model');

//update password
module.exports = async (req, res, next) => {
  try {
    const { app, body } = req;
    const User = app.service('users');
    const { uid, token, password } = body;

    if (!password) {
      return res.status(400).json({code:'PASSWORD_REQUIRED'});
    }

    const userInstance = await UserModel.findByPk(uid);

    if (!userInstance || !token || (userInstance.reset_token != token)) {
      return res.status(403).json({code:'INVALID_LINK'});  
    }
    if (moment().isAfter(moment(userInstance.reset_token_expiry))) {
      return res.status(403).json({code:'RESET_PASSWORDLIKK_EXPIRED'});    
    }

    let patchData = {
      password,
      reset_token_expiry: null,
      reset_token: null
    };

    await User.patch(uid, patchData);
    res.send({ 'status': 'SUCCESS', message: 'PASSWORD_UPDATE_SUCCESS' });
  } catch (e) {
    return next(e);
  }
};
