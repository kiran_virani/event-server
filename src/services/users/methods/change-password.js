const bycrypt = require('bcryptjs');
const logger = require('../../../logger');

//change password
module.exports = async (req, res, next) => {
  try {
    const { app, user, body } = req;
    const { oldPassword, newPassword } = body;
    const User = app.service('users');
    if (oldPassword == newPassword) {
      return res.status(400).send({ status: 'FAIL', message: 'USE_DIFFRENT_PASSWORD' });
    }
    //compare encrypted password
    bycrypt.compare(oldPassword, user.password, async (err, result) => {
      if (err) {
        logger.error('ERROR > ', err);
        return res.send(`Error ${err}`);
      }
      if (result) {
        try {
          let id = user.id.toString();
          let patchData = { password: newPassword };
          await User.patch(id, patchData);
          res.send({ 'status': 'SUCCESS', message: 'PASSWORD_CHANGED_CUCCESSFULLY' });
        } catch (e) {
          app.error(e);
          return next(e);
        }
      } else {
        res.status(400).send({ status: 'FAIL', message: 'OLD_PASSWORD_DOES_NOT_MATCH' });
      }
    });
  } catch (e) {
    return next(e);
  }
};
