const UserModel = require('../../../models/user.model');
const _ = require('lodash');

const list = async (req, res, next) => {
  try {
    const { user } = req;

    let _query = {
      where: {
        id: {
          $ne: user.id
        }
      }
    };

    const usersList = await UserModel.findAll(_query);
    const usersCount = await UserModel.count({ where: _query.where });
    const resp = {
      total: usersCount,
      data: usersList
    };
    res.status(200).send(resp);
  } catch (error) {
    return next(error);
  }
};

module.exports = list;