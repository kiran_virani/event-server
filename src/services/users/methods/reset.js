const moment = require('moment');
const randomstring = require('randomstring');

const RESET_EXPIRY_TTL = 15;

const reset = async (req, res, next) => {
  try {
    const { app, body } = req;
    const { email } = body;
    const sequelize = req.app.get('sequelizeClient');
    const User = app.service('users');
    const {
      user: UserModel
    } = sequelize.models;

    if (!email) {
      return res.status(422).json({code:'EMAIL_REQUIRED'});
    }
    const userInstance = await UserModel.findOne({ where: { email } });
    if (!userInstance) {
      return res.status(404).json({code:'EMAIL_NOT_REGISTERED'});
    }
    const patchData = {
      reset_token: randomstring.generate(64),
      reset_token_expiry: moment().add(RESET_EXPIRY_TTL, 'minutes').toISOString()
    };
    console.log('id====', userInstance.id);
    console.log('Patch====', patchData);
    const data = await User.patch(userInstance.id, patchData);
    console.log('Response====', data);    
    let successObj = {
      status: 'SUCCESS',
      message: 'RESET_PASSWORD'
    };

    return res.status(200).json(successObj);
  } catch (exec) {
    return next(exec);
  }
};

module.exports = reset;
