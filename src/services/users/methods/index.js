const create = require('./create');
const list = require('./list');
const changePassword = require('./change-password');
const reset = require('./reset');
const verifyResetToken = require('./verify-reset-token');
const resetPassword = require('./reset-password');

const Methods = {
  create,
  list,
  changePassword,
  reset,
  verifyResetToken,
  resetPassword
};

module.exports = Methods;
