const _ = require('lodash');        
// const Message = require('../../../utils/messages');
const logger = require('../../../logger');

const ALLOWED_VALUES = ['first_name', 'last_name', 'email', 'password'];

const create = async (req, res, next) => {
  try {
    const { app, body } = req;
    const User = app.service('users');
    let data = _.pick(body, ALLOWED_VALUES);

    const userInstance = await User.find({ query: { email: data.email, $limit: 1 } });
    if (userInstance.data.length) {
      return res.status(409).json({code:'EMAIL_ALREADY_EXIST'});
    }

    const _userInstance = await User.create(data);
    // Message.setResponseHeader('USER_CREATED_SUCCESS', res, req);

    let successObj = {
      data: {
        user: _userInstance
      },
      status: 'SUCCESS',
      message: 'Usesr created successfully'
    };
    return res.status(200).json(successObj);
  } catch (exec) {
    logger.error('ERROR > USER > CREATE > ', exec);
    return next(exec);
  }
};

module.exports = create;
