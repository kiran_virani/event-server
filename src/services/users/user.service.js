// Initializes the `users` service on path `/users`
const createService = require('feathers-sequelize');
const UserModel = require('../../models/user.model');
const hooks = require('./user.hooks');
const UserRoutes = require('./user.route');
const sequelizeOperatorAliases = require('../../sequelize-operators-alises');

module.exports = function (app) {
  const options = {
    name: 'users',
    Model: UserModel,
    paginate: app.get('paginate'),
    whitelist: Object.keys(sequelizeOperatorAliases)
  };

  // Initialize User Routes
  app.configure(UserRoutes);

  // Initialize our service with any options it requires
  app.use('/users', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('users');

  service.hooks(hooks);
};
