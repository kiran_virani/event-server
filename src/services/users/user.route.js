const UserMethods = require('./methods');
const { authenticate } = require('@feathersjs/express');

module.exports = function (app) {
  app.get('/users/list', authenticate('custom'), UserMethods.list);
  app.post('/users/create', UserMethods.create);
  app.post('/users/change-password', authenticate('custom'), UserMethods.changePassword);
  app.post('/users/reset', UserMethods.reset);
  app.get('/users/verify-reset-token', UserMethods.verifyResetToken);
  app.post('/users/reset-password', UserMethods.resetPassword);
};
