// Initializes the `event` service on path `/event`
const { Event } = require('./event.class');
const EventModel = require('../../models/event.model');
const hooks = require('./event.hooks');
const EventRoutes = require('./event.route');
const sequelizeOperatorAliases = require('../../sequelize-operators-alises');

module.exports = function (app) {
  const options = {
    name: 'event',
    Model: EventModel,
    paginate: app.get('paginate'),
    whitelist: Object.keys(sequelizeOperatorAliases)
  };

  // Initialize User Routes
  app.configure(EventRoutes);

  // Initialize our service with any options it requires
  app.use('/event', new Event(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('event');

  service.hooks(hooks);
};
