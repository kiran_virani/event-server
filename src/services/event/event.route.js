const EventMethods = require('./methods');
const { authenticate } = require('@feathersjs/express');

module.exports = function (app) {
  app.post('/event/create', authenticate('custom'), EventMethods.create);  
  app.get('/event/list', authenticate('custom'), EventMethods.list);
  app.get('/event/:id/details', authenticate('custom'), EventMethods.eventDetails);
  app.put('/event/:id/update', authenticate('custom'), EventMethods.update);
};
