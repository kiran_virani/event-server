const create = require('./create');
const list = require('./list');
const update = require('./update');
const eventDetails = require('./event-details');

const Methods = {
  create,
  list,
  update,
  eventDetails
};

module.exports = Methods;
