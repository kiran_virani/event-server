const _ = require('lodash');
const logger = require('../../../logger');

const ALLOWED_VALUES = ['user_id', 'event_name', 'event_date', 'description', 'invited_users'];

const create = async (req, res, next) => {
  try {
    const { app, body } = req;
    const sequelize = app.get('sequelizeClient');
    const {
      event: EventModel
    } = sequelize.models;

    let data = _.pick(body, ALLOWED_VALUES);

    //CREATE EVENT
    const eventInstance = await EventModel.create(data);

    let successObj = {
      status: 'SUCCESS',
      message: 'EVENT_CREATE_SUCCESS',
      data: eventInstance
    };
    return res.status(200).json(successObj);
  } catch (exec) {
    logger.error('ERROR > EVENT > CREATE > ', exec);
    return next(exec);
  }
};

module.exports = create;
