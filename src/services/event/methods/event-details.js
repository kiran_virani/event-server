const _ = require('lodash');

const eventDetails = async (req, res, next) => {
  try {
    const { app } = req;
    const sequelize = app.get('sequelizeClient');
    const {
      event: EventModel,
      user: UserModel,
    } = sequelize.models;

    let eventInstance = await EventModel.findOne({
      where: { id: req.params.id },  
      include: [
        {
          model: UserModel,
          as: 'user'
        }
      ]
    });

    // if (!eventInstance) {
    //   return res.status(404).send({ status: 'FAIL', message: 'NOT_FOUND' });  
    // }

    // //update user
    // await EventModel.update(update_data, { where: { id: req.params.id } });

    let successObj = {
      status: 'SUCCESS',
      message: 'EVENT_UPDATE',
      data: eventInstance
    };

    res.status(200).json(successObj);

  } catch (error) {
    return next(error);
  }
};

module.exports = eventDetails;
