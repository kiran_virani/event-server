const EventModel = require('../../../models/event.model');
const _ = require('lodash');

const list = async (req, res, next) => {
  try {
    const { user, query } = req;

    let _query = {};
    _query = {
      where: {
        $or: [
          {
            user_id: user.id
          },
          {
            invited_users: {
              $contains: [user.id]
            }
          }
        ]
      }
    };

    if (query.name) {
      _query.where.event_name = {$like: `%${query.name}%`};
    }
    if (query.start_date && query.end_date ) {
      _query.where.event_date= {
        $gte: new Date(query.start_date),
        $lte: new Date(query.end_date)
      };
    }

    const eventList = await EventModel.findAll(_query);
    const eventCount = await EventModel.count(_query);
    const resp = {
      total: eventCount,
      data: eventList
    };
    res.status(200).send(resp);
  } catch (error) {
    return next(error);
  }
};

module.exports = list;