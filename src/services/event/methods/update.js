const _ = require('lodash');

const ALLOWED_VALUES = ['event_name', 'event_date', 'description'];

const update = async (req, res, next) => {
  try {
    const { app, body } = req;
    const sequelize = app.get('sequelizeClient');
    const {
      event: EventModel,
    } = sequelize.models;

    let update_data = _.pick(body, ALLOWED_VALUES);

    let eventInstance = await EventModel.findByPk(req.params.id);

    if (!eventInstance) {
      return res.status(404).send({ status: 'FAIL', message: 'NOT_FOUND' });  
    }

    //update user
    await EventModel.update(update_data, { where: { id: req.params.id } });

    let successObj = {
      status: 'SUCCESS',
      message: 'EVENT_UPDATE'
    };

    res.status(200).json(successObj);

  } catch (error) {
    return next(error);
  }
};

module.exports = update;
