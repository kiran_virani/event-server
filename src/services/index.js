const users = require('./users/user.service.js');
const event = require('./event/event.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(event);
};