const Sequelize = require('sequelize');
const CONSTANTS = require('./constants.js');
const POSTGRES = CONSTANTS.POSTGRES;

const operatorsAliases = require('./sequelize-operators-alises');

const sequelizeClient = new Sequelize({
  host: POSTGRES.HOST,
  username: POSTGRES.USERNAME,
  password: POSTGRES.PASSWORD,
  database: POSTGRES.DB,
  dialect: POSTGRES.DIALECT,
  port: POSTGRES.PORT,
  logging: false,
  operatorsAliases,
  define: {
    freezeTableName: true
  }
});

module.exports = sequelizeClient;
